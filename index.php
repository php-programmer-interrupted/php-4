<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Задание №1</title>
    <h1>ФОТОГАЛЕРЕЯ</h1>
</head>
<body>
    <?php
        $url_parts = explode('/', $_SERVER['REQUEST_URI']);
        
        // Отображение каталога только в корневой папке
        if ($_SERVER['REQUEST_URI'] === '/')    // Если запрошен корень сайта
        {
    
            // Вывод списка фотографий
            for ($i = 1; $i <= 9; $i++)
            {
                ?>
                <a href="/photo/<?php echo $i ?>">Фотография №<?php echo $i ?></a><br>
        <?php
            }
        }
        else
        {
            // Если строка запроса начинается с photo, то можем обработать скриптом photo.php
            if (strpos($_SERVER['REQUEST_URI'],'/photo/') === 0)
            {
                include("photo.php");
            }
            else
            {
                echo "Ошибка 404";
            }
        }
        ?>
</body>
</html>