<?php
/**
 * Скрипт отображает фотографию из папки img
 */
    //if (isset($_GET['id'])) $photo_id = (int) $_GET['id'];    // 1-й способ
    $photo_id = (int) $url_parts[2];                            // 2-й способ
    // Обработка корректности ввода ID
    /*
    if (strlen($photo_id) != strlen($_GET['id'])) {
        die("Ошибка! Не корректный ID!");
    }
    */
    // Обработка существования ID
    if (($photo_id > 9) OR ($photo_id < 1)) {
        die("Ошибка! Не существующий ID!");
    }
?>
<h1>Фотография №<?php echo $photo_id; ?></h1>
<img src="/img/<?php echo $photo_id; ?>.jpg" alt="Фотография №<?php echo $photo_id; ?>">
<br>
<a href="/"><< Назад</a>
